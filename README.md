# Python 3 tutorial

This is a workshop I have given at Goettingen University for PhD students of
various fields related to experimental psychology.

## Goals

The students wanted to be taught 'python 3 in general'. As it is very hard to
know what the students already know and - as a consequence - how well they know
and how well they can express what they need, I have tried to do the following:

1. Teach the students python as a general purpose language for manipulating
   files and file contents. Examples for this are tailored to tasks that I had
   to do during my PhD time.
2. Still teach the students python specifically for science. In a lot of places
   it makes no sense to try and solve problems using general python instead of
   the nice data science packages it already offers.

The general structure follows the entire process of an entire research cycle in
Psychology. There are scripts to create trial materials, to run an experiment
and to analyze the data (next to a script for generating fake data so analysis
can be practiced).

# Notes

## Open TODOs

* Make branch without tasks solved
* Write a 'slide' with what to talk about for each lesson

## Things to talk about

* Who am I and what qualifies me to give the class
    * What I do now and how python relates to it
    * What I have done before and how learning python was related to it
* Where do we get the data (TODO: put it on a stick as well)
* Using jupyter as an abstraction away from os (wherever possible)

# 'License'

Use it as you see fit. Mentioning my name is appreciated but not necessary. As
long as this is reused and helps anybody I am grateful.

Dr. Florian Kornrumpf
