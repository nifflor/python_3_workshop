#!/usr/bin/python3
"""
This is just a single script to be imported from somewhere else
"""

def helper_fun(integer):
    """
    Tremendously useful helper that always adds 2 to an integer
    """
    return integer + 2
