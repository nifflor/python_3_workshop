# Make sure all subparts are included
from . import utils # a file is a submodule
from . import submodule # a folder is too!

# Make sure that `from PackageToImport import *` works
__all__ = ['utils', 'submodule']
