"""
Private submodule that only contains functions that are needed internally for
the entire package to function but should normally not be imported.
"""

def obscure_low_level_helper(integer):
    if integer // 2 == 0:
        return integer + 1
    else:
        return integer
