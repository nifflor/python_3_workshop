from . import private_internal_helpers as pih

def my_even_helper(integer):
    return pih.obscure_low_level_helper(integer) + 4
