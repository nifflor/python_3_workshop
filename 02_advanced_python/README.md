# Rationale

This folder is intended to show you some more advanced features of python. The

1. Objects
2. Importing your own packages
3. Interacting with `sys` and running your scripts from a shell

## 1. Objects

Python _objects_ and show you the 'real' way python is intended to be used. You
will most likely not use this feature for scientific programming where python
is used as a scripting language and objects are rarely defined in your own
code. Still, understanding objects is very helpful in understanding how other
python packagges function.

## 2. Importing you own packages

We will see how to write your own code and organize it into different files. We
will then see how importing one script into another is no different from
importing other peoples packages. We will also get an idea where everything is
stored in you system so you understand python completely.

## 3. Interacting with `sys` and running your scripts from a shell

We will see how to run a python script in it's most basic version. Before
_jupyter_ came around, this was how I ran all my code and it definitely has
it's place still.
