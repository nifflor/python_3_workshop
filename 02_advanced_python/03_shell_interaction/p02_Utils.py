#!/usr/bin/python3
"""
Now, let's suppose we wrote a set of great utility functions. There are two
different use-cases for it:

1. Sometimes, we want to run it from the shell using `sys`
2. Sometimes, we want to import it in other scripts

Problem: During the import, all code is executed, but nothing is provided via
`sys`. So calls to `sys.argv` will fail.
"""

## running scripts from shell 'properly'
import sys

# let's define a function to make use of this
def print_to_file(filename):
    """
    Prints to a file that we pass
    """
    with open(filename, 'w') as outfile:
        outfile.write('This is very useful but repetitive to type newly every time')

# try importing this file, see how it fails. Then comment this line
#print_to_file(sys.argv[1])

# Solution: The following construct tests if the script is directly called from
# the shell (i.e. the _main_ script that we are running). The code below it is
# only executed in such cases, but not during imports of this file in other
# scripts:

if __name__ == '__main__':
    print_to_file(sys.argv[1])
