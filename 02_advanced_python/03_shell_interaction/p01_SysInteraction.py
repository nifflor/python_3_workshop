#!/usr/bin/python3
"""
A minimal example of how to run things from the shell with arguments

Problem: We want to run a script from the shell with `python3
[script_name].py`, but we need to pass it arguments from outside.
"""

# let's define a utility function that we need every once in a while to create
# a file with useful info
def print_to_file(filename):
    """
    Prints to a file that we pass
    """
    with open(filename, 'w') as outfile:
        outfile.write('This is very useful but repetitive to type newly every time')

# now let's use it to write to a file whose name we know
print_to_file('Filename_from_within_the_script.txt')

# Problem: Everytime we want to change the filename, we need to enter this
# file, change the name and then re-execute the script. Hardly a
# convenience/utility script.
# Solution: Let's pass the data from outside the script while calling it by
# using the `sys` package
import sys

# here is how it get's arguments
print('Args: ' + ','.join(sys.argv))
print('This shows only a single argument: {0}'.format(sys.argv[1]))

# now we don't have to change the script if we want to write to another file,
# we just pass it as an argument to the script on calling it.
print_to_file(sys.argv[1])
