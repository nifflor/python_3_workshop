# Why psychopy?

It is infeasible to just write an experiment using 'pure' python. Some packages
are always needed anyways. As an example, you could start using 'pygame' to
show things on a screen. But then, you would have to get into the gory details
of understanding how your screen works: 

* Is it a TFT? Is it a CRT?
* How long do phosphor ions take to be unlit after the cathode ray hit them?
* How soon after I turn a pixel on a TFT 'on', does it really reach it's full
  luminance?
* How does a graphic card's front and backbuffer system work?
* How in windows different than MacOS if at all?

The list goes on. Someone (Jonathan Peirce) already spent years of his time
solving all of these complicated issues. And many many more. So I would
strongly encourage you to profit from his work!


# Psychopy and python 3

There is/ will be psychopy with python 3, but not for linux yet:

https://github.com/psychopy/psychopy/releases/tag/3.0.0b7

python 2 and 3 are not that different in many aspects that are relevant for us.
It is well possible to use python3 in general but write your experiment script
writing python 2. A few glitches might happen, but nothing serious.
