# -*- coding: utf-8 -*-
"""
@author: Dr. Florian Kornrumpf
@contact: nifflor@googlemail.com
@date: 2018-11-21
"""

from psychopy import visual, core, event, gui, data
import codecs, os
import random

# ==============================================================================
# global variables: INTERFACE
# ==============================================================================

PATH = '../01_basic_python_science_utility/trial_materials_ef/'
OUTPATH = './results/' # where experiment data is written

SCREEN_SIZE = [1366, 768]  # what is your screen resolution?
TEXT_SIZE = 40

# ==============================================================================
# preparations
# ==============================================================================

# gather experiment and subject information
exp_name = 'Py3WSEF' # python 3 workshop eriksen flanker
exp_info = {'Subject': '', 'Subject (repeat)': ''}

# prompt user for subject number
dlg = gui.DlgFromDict(dictionary=exp_info, title=exp_name)
if dlg.OK is False:
    core.quit()  # user pressed cancel

# if repetition and original do not match, repeat prompt
while exp_info['Subject'] != exp_info['Subject (repeat)']:
    dlg = gui.DlgFromDict(dictionary=exp_info, title='Please insert matching number in both fields')
    if dlg.OK is False:
        core.quit()  # user pressed cancel

# dictionary with additional info about the experiment
exp_info['date'] = data.getDateStr()  # add a simple timestamp
exp_info['exp_name'] = exp_name

# define output filename
output_file = OUTPATH + exp_info['exp_name'] + '_{0:02d}.csv'.format(int(exp_info['Subject']))
# make sure that we don't overwrite existing data!
if os.path.isfile(output_file):
    print 'Error: File already exists. Restart experiment and choose subject number which is not used yet.'
    exit(1)

# create a window
exp_win = visual.Window(size=SCREEN_SIZE, monitor="testMonitor", color=(230, 230, 230),
                        colorSpace='rgb255', units="pix")

# feedback screens for practice trials
correct_screen = visual.TextStim(exp_win, pos=[0, 0], text='Richtig!', font='Arial', color='green',
                            height=TEXT_SIZE, alignHoriz='center', units=u'pix')
incorrect_screen = visual.TextStim(exp_win, pos=[0, 0], text='Leider falsch.', font='Arial', color='red',
                            height=TEXT_SIZE, alignHoriz='center', units=u'pix')

stimulus = visual.TextStim(exp_win, pos=[0, 0], text='A', font='Courier New',
        color='black', height=25, alignHoriz='center', units=u'pix')
flanker = visual.TextStim(exp_win, pos=[0, 0], text='BBB BBB', font='Courier New',
        color='black', height=25, alignHoriz='center', units=u'pix')

#reaction time clock
rt_clock = core.Clock()


# ==============================================================================
# read stimuli
# ==============================================================================

def read_stims(stim_file):
    """
    read stimulus info from file and put into a list
    a list of lists containing trials
    """
    trial_list = []
    with codecs.open(stim_file, 'rb', encoding="utf-8") as infile:
        for line in infile:
            line = line.strip()
            if 'Subject' in line:  # its the header
                continue
            elif len(line) == 0:  # last line if an empty one
                break
            line = line.split(',')  # split line into columns
            trial_list.append(line)  # write line to item list

    # shuffle list
    random.shuffle(trial_list)

    return trial_list

# use the function to actually read trial and practice lists
prac_list = read_stims('{0:s}/Practice_{1:02d}.csv'.format(PATH,
    int(exp_info['Subject'])))
trial_list = read_stims('{0:s}/Subject_{1:02d}.csv'.format(PATH,
    int(exp_info['Subject'])))


# ==============================================================================
# define trial procedure
# ==============================================================================

def run_trial(trial_info_list):
    """
    Runs a single trial
    """

    # read relevant info from trial list
    stimulus.text = trial_info_list[5]
    flanker.text = trial_info_list[6] * 3 + ' ' + trial_info_list[6] * 3
    corr_ans = trial_info_list[7]

    event.clearEvents() # clear old user responses
    exp_win.flip()
    core.wait(.2) # inter trial interval

    flanker.draw()
    stimulus.draw()
    exp_win.flip()
    rt_clock.reset()

    # collect answer
    # OPTION 1: use the wrapper already existent (does not work with pygame)
    #answer, rt = event.waitKeys(keyList=['s', 'l'], timestamped=rt_clock)

    # OPTION 2: do it yourself
    press_list = []
    # wait for click and check which button is pressed
    while len(press_list) == 0: # no answer given yet
        press_list = event.getKeys(keyList=['s', 'l'])

    rt = rt_clock.getTime()
    answer = press_list[0]

    # check if answer is correct
    corr = answer == corr_ans

    # present feedback in practice trials
    if practice is True:
        if corr:
            correct_screen.draw()
            exp_win.flip()
            core.wait(1)
        else:
            incorrect_screen.draw()
            exp_win.flip()
            core.wait(1)

    # return results
    return answer, corr, rt


# ==============================================================================
# run experiment
# ==============================================================================

# ------------------------------------------------------------------------------
# present instructions

# reversed mapping from trial list creation
stimulus_answer_mapping = [{'s':'B', 'l':'Z'},{'s':'Z', 'l':'B'}]
this_subject_map = stimulus_answer_mapping[int(trial_list[0][4])] # stimulus map value in any trial list

print(this_subject_map)

instructions_text = '''
Welcome to the experiment. Thank you for being here.

Please put your left and right index fingers on the 's' and 'l' keys respectively.
These are your answer keys during the trial.
In each trial, you will see 7 letters in the screen center. Your task to pay only
attention to the central of these letters and ignore the rest. If the central 
letter is '{0:s}' then press 's', if the central letter is '{1:s}' then press 'l'.

Try to answer as accurately and quickly as possible. During the first eight trials
you will receive feedback, during the second eight trials there will be no feedback.

Press space to continue to the experiment.
'''.format(this_subject_map['s'], this_subject_map['l'])

# instructions that the test is done
visual.TextStim(exp_win, pos=[0, 0],
    text=instructions_text, font='Arial',
    color='black', height=25, alignHoriz='center', units=u'pix').draw()
exp_win.flip()
# wait for key press to continue
event.waitKeys()


# ------------------------------------------------------------------------------
# run experiment
with codecs.open(output_file, 'ab', encoding="utf-8") as outfile:
    # write outfile header
    outfile.write('Subject,Trial,Direction,Congruency,StimulusMap,Stimulus,Flanker,CorrectAnswer,GivenAnswer,Ans_correct,PracticeTrial,RT\n')

    # run practice trials
    practice = True
    for p_trial in range(len(prac_list)):
        # inter trial interval
        core.wait(.5)
        # run trial
        ans, corr, rt = run_trial(prac_list[p_trial])
        # write output
        outfile.write(
            u'{0:s};{1:s};{2:d};{3:d};{4:f}\n'.format(
            ','.join([str(x) for x in prac_list[p_trial]]), ans, corr, practice, rt))

    visual.TextStim(exp_win, pos=[0, 0],
        text='Press space to start the real experiment', font='Arial',
        color='black', height=25, alignHoriz='center', units=u'pix').draw()
    exp_win.flip()
    # wait for key press to move on
    event.waitKeys()
    # run real trials
    practice = False

    for trial in range(len(trial_list)):
        # inter trial interval
        core.wait(.5)

        # run trial
        ans, corr, rt = run_trial(trial_list[trial])

        outfile.write(
            u'{0:s};{1:s};{2:d};{3:d};{4:f}\n'.format(
            ','.join([str(x) for x in trial_list[trial]]), ans, corr, practice, rt))


# instructions that the test is done
visual.TextStim(exp_win, pos=[0, 0],
    text='Thanks for your participation', font='Arial',
    color='black', height=25, alignHoriz='center', units=u'pix').draw()
exp_win.flip()
# wait for key press to continue
event.waitKeys()

# close psychopy window
exp_win.close()

# quit experiment
core.quit()
