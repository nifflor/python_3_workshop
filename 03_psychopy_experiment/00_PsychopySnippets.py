"""
Snippets for psychopy things I had to do in my experiments. Hope they give a
starting point.
"""

### present instructions as a series of prepared pictures that fill the entire
# screen.
# instruction screen
PATH = 'path/to/a/folder/with/the/pictures'
files = [f for f in os.listdir('{0:s}/instr_pics/'.format(PATH)) if match(r'Instruction_Screen_[0-9]+.png', f)]

# present as many instructions as files matching the name pattern where in the folder
for file in files:
    instr_screen = visual.SimpleImageStim(exp_win, image='{0:s}/instr_pics/{1:s}'.format(PATH,file))
    instr_screen.draw()
    exp_win.flip()
    # wait for button press to move on
    event.waitKeys()

### use a picture
# load trial picture
trial_picture = visual.ImageStim(exp_win,
        image='{0:s}/stim_pics/{1:s}'.format(PATH, trial_picture_name),
        pos=[0, 100], units=u'pix')

### Build a few buttons to click as response
# create 3 buttons for response
BUTTON_LABELS = [u'Yes', u'No', u'Maybe'] # what the buttons say
BUTTON_POSITIONS = [(-300, -200), (0, -200), (300, -200)] # x and y position of buttons
button_list = [] # store the button text
button_area_list = [] # store the button boxes
for button_ix in range(len(BUTTON_LABELS)):
    button_list.append(visual.TextStim(exp_win, text=BUTTON_LABELS[button_ix],
                                       font='Arial',
                                       height=32,
                                       alignHoriz='center',
                                       pos=BUTTON_POSITIONS[button_ix],
                                       color='black'))

    button_area_list.append(visual.Rect(exp_win,
                                        lineColor=BUTTON_COLORS[button_ix],
                                        fillColor=None,
                                        height=40,
                                        width=250,
                                        pos=BUTTON_POSITIONS[button_ix]))


# later during trial: draw the buttons
for button_area in button_area_list:
    button_area.draw(exp_win)

# draw the labels on the buttons
for button in button_list:
    button.draw(exp_win)


## use a mouse as response device
mse = event.Mouse(visible=True, win=exp_win)



### Eye tracking snippets
# tracker implementation really depends on the tracker. They should have
# documentation on their websites. Google: [tracker provider] python API
# often there is some way to create a python object that is the tracker

# example for the eyetribe tracker
tracker = EyeTribe(logfilename=et_filename)
# then the tracker can do things with pre-defined methods like
tracker.calibrate(...)
tracker.start_recording()
tracker.log_message("trial_start") # write message to recorded data
tracker.sample() # use a sample to see where a person is looking at the moment
tracker.stop_recording()
tracker.close()

# general helpers
# fixation cross
fix_cross = visual.TextStim(exp_win, pos=[0, 0], text='+', font='Arial', color=-1,
                            height=FIXCROSS_SIZE, alignHoriz='center', units=u'pix')

# recalibration instructions
recal_text = visual.TextStim(exp_win, pos=[0, 0],
                             text='Der Blickmesser muss neu kalibriert werden.\n' +
                                  'Melde Dich bitte bei der Versuchsleitung.\n' +
                                  'Versuchsleiter: Rekalibrieren und weiter mit "g".',
                             font='Arial', color=-1, height=FIXCROSS_SIZE,
                             alignHoriz='center', units=u'pix')

