#/usr/bin/python
# -*- encoding -*-: utf-8
"""
File to ensure that everyone is at a consistent level of python knowledge so
that we can start the workshop well grounded.

Goal is that you have python installed and are able to execute this script and
understand each line in this script. If you don't understand what a line does,
do the following:

1. Try to execute it and understand the result. If it is a line consisting of a
complex combination of simpler parts, try to pick it apart and run the simpler
parts until you understand all of them. Then recombine them to build up the
complete expression.
2. If you get stuck or have arrived at the simplest possible expression and
can't continue with strategy 1, then google it. I will try to provide hints of
what to google in many lines. If there is no hint, google the contents of the
line itself. If you try to do something and get an error message in python,
google the error message.

These two strategies are literally how I learned python (and many other
programming languages). But both steps require practice. In the beginning, an
explanation by google will only raise more questions. Follow through! The more
you do that, the easier it will become. If you are good at this, then you don't
need me or anyone to improve your python skills.

NOTE: Some places in this script mention the python package manager `pip` to
install packages. Get it, unless you installed python with `Anaconda` (a
scientific python version that comes with many packages pre-installed).
Anaconda has it's own package manager `conda`. I think you can replace any
instance of `pip` with `conda` and you will be fine.
"""

## basic data types and operations
# types for numbers: intergers and floats
my_int = 1
my_float = 1.4

# if you did 2/3 in python 2, the answer is `0`
# google: python 3 integer division
2/3
2//3
2/3.0
2//3.0

my_int + 1
my_int
my_int = my_int + 1
my_int += 1 # google: python +=

4%3 # google: modulo operator
1%3
3%3

# google: python3 string
my_string = 'abc'
my_other_string = "def"
my_third_string = 'this contains the other ones: "". you can do the inverse as well'

my_long_string = '''
the cat
sat on
the mat
'''

my_string + '   ' + my_other_string # you can 'add' strings!

# try to put an uppercase string to lower. guess based on this or google:
# python3 turn string to lowercase
my_string.upper()

my_long_string
print(my_long_string)

my_long_string.strip() # google: python string.strip()
my_long_string.strip().strip('t')

# booleans: needed for logic and control flow of programs
my_boolean = True # google: boolean expression
not my_boolean
False and my_boolean
False or my_boolean
# use for control flow will come below with `if` and `while`

# optional: google python xor


## collection data types
my_list = [1, 2, "any", "other", ["datatype"], 3]

# google: python indexing lists
my_list[0]
my_list[1:3]

my_list.append(3)
my_list.append([3])
my_list.extend([3])
my_list + [2, 3, 5] # you can 'add' lists!

my_dictionary = {"a": 1, "b":'c', 'd':my_list}

# google: python get from dictionary
my_dictionary['a']
#my_dictionary['z'] # would throw an error. try it
my_dictionary.get('a', 'not found')
my_dictionary.get('z', 'not found') # no error!

# google: python add key to dictionary
my_dictionary['z'] = 1

# google: python overwrite key in dictionary
my_dictionary['b'] = 4


## variables
# we have already used them: containers to reference anything we want to keep
a = 'bla'
# `a` is your variable, it can store any of the data types that we know
# by now and others. You can overwrite it and store something else in it.


## functions vs methods

a = 'i am a string'
print(a) # apply a function to a data type
a.upper() # apply a method for strings to a string
# why is it print(a) but a.upper()?
# OPTIONAL! if you are insterested:
# google: object oriented programming
# google: oop function vs method
# (oop means object oriented programming)
# For the course and your academic use of python it is probably sufficient for
# you to know that both 'do' something with data types. Methods do something
# specifically for the data type they belong to, functions are usually more
# general..

# google: python define functions
def my_add1_function(number):
    # str(number): google python cast int to string
    print('Adding 1 to ' + str(number))
    return number + 1 # functions should always return something!

my_result = my_add1_function(3)


## control structures

# for: traverse or repeat fix amount of times
my_list = [1,2,3,4]

# indexing can help us do something with each element of the list:
my_add1_function(my_list[0]) # my_add1_function is defined above
my_add1_function(my_list[1])
my_add1_function(my_list[2])
my_add1_function(my_list[3])

# there is a better way to traverse the list that involves less repetition of
# code
for i in my_list:
    my_add1_function(i)

# we can also use them just to repeat stuff some amount of time
for i in range(50): # don't try to run range(50) on it's own.
    print('I will not swear in class')
    # here, we don't do anything with a data structure we are traversing
    # we simply use for for repetition

# if: do something based on certain conditions
age = 1
if age < 10:
    print('too small to ride the scooter')
elif age == 10:
    print('soon you may ride the scooter')
else:
    print('in you go')

# while: repeat something as long as a certain condition is true
a = 1
while a < 50:
    a += 1
    print(a)

# let's combine
age = 1
while age < 12:
    if age < 10:
        print('too small to ride the scooter')
    elif age == 10:
        print('soon you may ride the scooter')
    else:
        print('in you go')
    age += 1


## let's use cool stuff other people have written: modules/packages
# let's for example interact with our operating system (os)
import os # google: python import modules, what are python modules

os.getcwd() #google: python print current working directory

# you might have to install numpy. google: `pip install numpy`
# numpy is for linear algebra and is widely used in other packages, especially
# for data science
import numpy as np # python import with alias

np.ones([2,3]) #google: numpy matrix
# take-aways: import with an alias, use a module function with dot notation,
# having heard of numpy


## the most common task: traverse a list and create a list
# a list is pythons most idiomatic data structure and a for loop is it's most
# idiomatic control flow. We will use them all the time. And since this is so,
# there is an even more idiomatic construct to write them more quickly: the
# `list comprehension`
even_numbers = []
for i in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]:
    if i % 2 == 0:
        even_numbers.append(i)
    else:
        pass # these last two lines are unneccessary. find out why. google: pass

# now the same with a list comprehension
[i for i in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] if i % 2 == 0]

## final task
# install jupyter with `pip install jupyter`
# (maybe `pip install wheel` first)
# then run `jupyter notebook` and see if it works: you should see two tabs open
# in your browser. you can close them and press `ctrl-c` to stop the jupyter
# process. Nothing else required for now.
# If it does not work, please let me know via mail so we can get it up before
# the course starts.
